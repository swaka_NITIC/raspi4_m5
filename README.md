# RasPI4BとM5StickCPlusでRCクローラーを作るぞ

Ras.PI4BとM5StickCPlus間のBLE通信を利用してRCクローラーを作るプロジェクトです。

## 準備物
RCクローラー機体：  
・ なんちゃって自作クローラー。AliExp.とかで売ってるのでもOKかも。DCモーター＋ドライバをRas.PIから制御する。後でGPS,Lidar,距離センサ,カメラ等を追加していく予定。  

Rasp.PI4B（4GB):  
・Ubuntu Server 22.04LTS (64bit)  
・SDカード(minimum 16GB. SDXC-I A1U3以上の速いのが良い。さらに高耐久タイプであればなおOK) 
・SDカードにイメージを書き込むRaspbery PI Imagerの設定で、あらかじめSSHの有効化やSSID/PASSWORDの設定がインストール前に可能なのでやっておく(IP固定の設定はログイン後に実施)。   
・後からDesktopにしたい場合 → [Desktopifyを使う](https://gihyo.jp/admin/serial/01/ubuntu-recipe/0624)   
※ Desktop版はOSインストール時点で空きメモリ1GBくらいになり全体的に重い。Desktop環境不要ならServer版で十分。どうせSSHでログインするので。

M5StickCPlus:  
・とりあえず買ってきたままでOK。  

## Rasp.PIの環境準備(OSのインストール&設定)

1. [Raspberry PI Imager](https://www.raspberrypi.com/software/)のインストール  
2. Raspberry PI Imagerを起動し、OSを選定し、SDカードに書き込み  
3. WiFi接続でのIP固定
```
$ ip a
:
$ cd /etc/netplan
$ cp 50-cloud-init.yaml 99-netconfig.yaml
$ vi 99-netconfig.yaml
--------
network:
    version: 2
    wifis:
      wlan0:
        optional: false
        dhcp4: false
        dhcp6: false
        addresses: [192.168.0.101/24]
        gateway4: 192.168.0.1
        nameservers:
          addresses: [192.168.0.1, 8.8.8.8]
        access-points:
           "SSID":
           password: "PASSWORD" 
--------
$ sudo netplan apply
```  
4. BTの設定
```
$ sudo apt install raspi-config
$ sudo raspi-config 
→ シリアルコンソールを無効化

$ sudo apt install pi-bluetooth bluetooth bluez blueman 
$ sudo reboot

確認
$ bluetoothctl scan on
→  sudo bluetoothctl show で以下のように出てくればOK  
$ sudo bluetoothctl show

Controller DC:A6:32:71:3F:F3 (public)
        Name: raspi4b
        Alias: raspi4b
        Class: 0x006c0000
        Powered: yes
        Discoverable: no
        DiscoverableTimeout: 0x000000b4
        Pairable: no
        UUID: A/V Remote Control        (0000110e-0000-1000-8000-00805f9b34fb)
        UUID: Handsfree Audio Gateway   (0000111f-0000-1000-8000-00805f9b34fb)
        UUID: PnP Information           (00001200-0000-1000-8000-00805f9b34fb)
        UUID: Audio Sink                (0000110b-0000-1000-8000-00805f9b34fb)
        UUID: Headset                   (00001108-0000-1000-8000-00805f9b34fb)
        UUID: A/V Remote Control Target (0000110c-0000-1000-8000-00805f9b34fb)
        UUID: Generic Access Profile    (00001800-0000-1000-8000-00805f9b34fb)
        UUID: Audio Source              (0000110a-0000-1000-8000-00805f9b34fb)
        UUID: Generic Attribute Profile (00001801-0000-1000-8000-00805f9b34fb)
        UUID: Device Information        (0000180a-0000-1000-8000-00805f9b34fb)
        Modalias: usb:v1D6Bp0246d0540
        Discovering: no
        Roles: central
        Roles: peripheral
Advertising Features:
        ActiveInstances: 0x00 (0)
        SupportedInstances: 0x05 (5)
        SupportedIncludes: tx-power
        SupportedIncludes: appearance
        SupportedIncludes: local-name

たまにboot時にhciuartの起動に失敗する。手動で起動する。
$ sudo systemctl start hciuart.service

```  

5. SSHの設定（インストール時に設定していれば不要）
```
$ sudo apt install openssh-server net-tools
$ sudo systemctl enable sshd.service
$ sudo systemctl start sshd.service
```
6. GPIO関連の準備
カーネル5.11以降、GPIOの利用の仕方が変わったとのこと（https://ubuntu.com//blog/raspberry-pi-gpio-support-in-ubuntu）
LGPIOというpython3のパッケージがあるらしい。
```
sudo apt-get install git build-essential libglib2.0-dev

(raspi-config)
sudo apt install raspi-config

(i2c)
sudo apt install i2c-tools

(gpio)
sudo apt install python3-dev python3-pip
sudo pip3 install lgpio

(BLE)
sudo pip3 install bluepy  → 沢山あるが、今回はこちらを利用する
sudo pip3 install pybluez
sudo pip3 install adafruit-blinka-bleio
sudo pip3 install adafruit-circuitpython-ble
sudo pip3 install bleak
```


## M5StickCPlus側の準備
1. [Arduino IDE 1.8.xのインストール](https://www.arduino.cc/en/software)（2022/11時点）  
→ 2.0系列も出ていますが、M5シリーズの開発には1.8系列のほうが安定しているのでこちらをお勧めします。    
→ Scrachライクな[UIFlow](https://flow.m5stack.com/)や、VScode上の[Platform IO](https://platformio.org/)での開発も可能です。 
2. [M5シリーズのライブラリ導入、ボードマネージャ登録](https://docs.m5stack.com/en/quick_start/m5stickc_plus/arduino)  

## プログラム開発
M5StickCPlusをコントローラーとし、Ras.PI4をRC本体側に乗せることを想定します。Rasp.PI側ではM5StickCからの制御コマンドや加速度等のセンサーデータを送ります。

### BLE通信（M5StickCPlus: periferal）
[M5SCP_RPI4B_srv.ino](code/M5SCP_RPI4B_srv.ino)


### BLE通信 (Rasp.PI4B: central → 利用するbluepyライブラリはcentralの機能だけしかない？)
[connect.py](code/connect.py) (接続テスト用。要 bluepy)  
[M5SCP_RPI4B_cli.py]()  




