/*　(Ref.)
 *  https://www.souichi.club/m5stack/m5stickc-ble/#%E3%82%BD%E3%83%BC%E3%82%B9%E3%82%B3%E3%83%BC%E3%83%89
 *  https://zoe6120.com/2020/12/29/1676/
 *  https://logikara.blog/arduino_bluetooth/#:~:text=%E3%83%9E%E3%82%B9%E3%82%BF%E3%83%BC%E5%81%B4%E3%80%8CM5StickC%E3%80%8D%E5%8F%B3%E5%81%B4%E9%9D%A2%E3%81%AE%E3%83%9C%E3%82%BF%E3%83%B3B%E3%82%92%E6%8A%BC%E3%81%99%E3%81%A8%E3%80%81%E3%82%B9%E3%83%AC%E3%83%BC%E3%83%96%E5%81%B4%E3%81%AB%E6%96%87%E5%AD%97%E5%88%97%E3%80%8CB%20ON%21%E3%80%8D%E3%81%8C%E9%80%81%E4%BF%A1%E3%81%95%E3%82%8C%E3%80%81%E4%B8%8A%E7%94%BB%E5%83%8F%E3%81%AE%E3%82%88%E3%81%86%E3%81%AB%E3%80%8CM5StickC,Plus%E3%80%8D%E3%81%AE%E6%B6%B2%E6%99%B6%E3%81%AB%E3%80%8CBT%3A%20B%20ON%21%E3%80%8D%E3%81%8C%E8%A1%A8%E7%A4%BA%E3%81%95%E3%82%8C%E3%81%A6%E6%9C%AC%E4%BD%93LED%E8%B5%A4%E3%81%8C%E6%B6%88%E7%81%AF%E3%81%97%E3%81%BE%E3%81%99%E3%80%82 
 */
 
#include <M5StickCPlus.h> //controller as Periferal 
#include <utility/MahonyAHRS.h> // GyroZ calibration

#include <BLEDevice.h> // Bluetooth Low Energy 
#include <BLEServer.h> // Bluetooth Low Energy
#include <BLEUtils.h> // Bluetooth Low Energy
#include <BLE2902.h> // Bluetooth Low Energy

#include <esp_sleep.h>　//deep sleepする場合に利用

#define SMODE  0 // stop mode
#define GMODE  1 // gesture mode
#define CMODE  2 // controler mode

#define LED_PIN 10   
#define LED_ON  LOW
#define LED_OFF HIGH

// mode status variable
uint8_t modeA = SMODE;  // BtnA, init as Stop mode
uint8_t modeB = 0;      // BtnB


float accX = 0.0F;
float accY = 0.0F;
float accZ = 0.0F;

float gyroX = 0.0F;
float gyroY = 0.0F;
float gyroZ = 0.0F;

float pitch = 0.0F;
float roll  = 0.0F;
float yaw   = 0.0F;

float temp = 0.0F;

// GyroZ calibration
float stockedGyroZs[256];
int   stockCnt           = 0;
float adjustGyroZ        = 0;
int   stockedGyroZLength = 0;

//#define T_PERIOD 10 // アドバタイジングパケットを送る秒数
//#define S_PERIOD 10 // Deep Sleepする秒数

// BLE
RTC_DATA_ATTR static uint8_t seq; // 送信SEQ
BLEServer *pServer = NULL;
BLECharacteristic* pCharacteristic = NULL;
bool deviceConnected = false;
bool oldDeviceConnected = false;
uint32_t value = 0;

uint16_t aX; // X軸加速度
uint16_t aY; // y軸加速度
uint16_t aZ; // y軸加速度
uint16_t vbat; // Bat.電圧
uint8_t btnAStat = 0; // ボタンAの状態(初期値0)
uint8_t btnBStat = 0; // ボタンBの状態(初期値0)


// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
#define SERVICE_UUID           "00000010-0000-1000-8000-00805F9B34FB"
#define CHARACTERISTIC_UUID_RX "00000011-0000-1000-8000-00805F9B34FB"
#define CHARACTERISTIC_UUID_TX "00000012-0000-1000-8000-00805F9B34FB"

// Callback関数 ・・・接続の状態をM5StickCPlusに返す。
class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };
    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();
      if (rxValue.length() > 0) {
        Serial.println("*********");
        Serial.print("Received Value: ");
        for (int i = 0; i < rxValue.length(); i++)
          Serial.print(rxValue[i]);
        Serial.println();
        Serial.println("*********");
      }
    }
};

void setup() {
  
    // GyroZ calibration
    stockedGyroZLength=sizeof(stockedGyroZs)/sizeof(int);

    // M5 init
    M5.begin();

    // LCD init
    M5.Axp.ScreenBreath(8); // 画面の輝度を下げる
    M5.Lcd.setRotation(1); // LCDの方向を変える
    M5.Lcd.setTextSize(3); // フォントサイズを3にする
    M5.Lcd.setTextColor(WHITE, BLACK); // 文字を白、背景を黒

    // IMU initi
    M5.IMU.Init();
  
    // LED init
    pinMode(LED_PIN, OUTPUT);
    digitalWrite(LED_PIN, LED_OFF);
  
    // BLE初期化
    BLEDevice::init("M5StickCPlus_controller"); // BLEデバイスを初期化

    // Create the BLE Server
    pServer = BLEDevice::createServer();
    pServer->setCallbacks(new MyServerCallbacks());

    // Create the BLE Service
    BLEService *pService = pServer->createService(SERVICE_UUID);
  
    // Create a BLE Characteristic
    pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID_TX,
                      // BLECharacteristic::PROPERTY_READ  |
                      // BLECharacteristic::PROPERTY_WRITE |   
                      BLECharacteristic::PROPERTY_NOTIFY  //|
                      //BLECharacteristic::PROPERTY_INDICATE
                    );

    // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.descriptor.gatt.client_characteristic_configuration.xml
    // Create a BLE Descriptor
    pCharacteristic->addDescriptor(new BLE2902());

    BLECharacteristic *pCharacteristic = pService->createCharacteristic(
        CHARACTERISTIC_UUID_RX,
        BLECharacteristic::PROPERTY_WRITE
    );
    
    pCharacteristic->setCallbacks(new MyCallbacks());
 
    // Start the service
    pService->start();

    // Start advertising
    //BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
    //pAdvertising->addServiceUUID(SERVICE_UUID);
    //pAdvertising->setScanResponse(false);
    //pAdvertising->setMinPreferred(0x00);  // set value to 0x00 to not advertise this parameter
    //BLEDevice::startAdvertising();
    pServer->getAdvertising()->start();
    Serial.println("Waiting a client connection to notify...");
}

void loop() {

  uint8_t buf[12]; //12byte
    
  M5.update();
  
  // データ取得
  M5.IMU.getGyroData(&gyroX, &gyroY, &gyroZ);
  M5.IMU.getAccelData(&accX, &accY, &accZ);
  M5.IMU.getAhrsData(&pitch, &roll, &yaw);
  // store gyroZ data for stockedGyroZLength times to calibration 
  if(stockCnt<stockedGyroZLength){
    stockedGyroZs[stockCnt]=gyroZ;
    stockCnt++;
  }else{
    if(adjustGyroZ==0){
      for(int i=0;i<stockedGyroZLength;i++){
        adjustGyroZ+=stockedGyroZs[i]/stockedGyroZLength;
      }
    }
    //avaraging stored gyroZ data to calibrate
    gyroZ-=adjustGyroZ; 
    MahonyAHRSupdateIMU(gyroX * DEG_TO_RAD, gyroY * DEG_TO_RAD, gyroZ * DEG_TO_RAD, accX, accY, accZ, &pitch, &roll, &yaw);
  }
  M5.IMU.getTempData(&temp);           
  
  aX = (int)(accX*100);
  aY = (int)(accY*100);
  aZ = (int)(accZ*100);
  // Bat.電圧チェック
  vbat = (uint16_t)(M5.Axp.GetVbatData() * 1.1 / 1000 * 100); // バッテリーの電圧
 
  // ボタンAによるmode変更
  if (M5.BtnA.wasReleased()) {   // ボタンAが押されたら
     btnAStat++;
     btnAStat = btnAStat % 3;
     Serial.printf("BtnA.wasPressed() %d \r\n", btnAStat);
  }
  // ボタンBによるmode変更
  if (M5.BtnB.wasReleased()) {   // ボタンBが押されたら
     btnBStat++;
     btnBStat = btnBStat % 4;
     Serial.printf("BtnB.wasPressed() %d \r\n", btnBStat);
     // Serial output mode change
      if ( btnBStat == 0 ) {
         Serial.printf("gyroX,gyroY,gyroZ\n");
      } else if ( btnBStat == 1 ) {
         Serial.printf("accX,accY,accZ\n");
      } else if ( btnBStat == 2 ) {
         Serial.printf("pitch,roll,yaw\n");
      } else if ( btnBStat == 3 ) {
         Serial.printf("temperature\n");
      }
  }
  
  // Screen出力
  M5.Lcd.setCursor(0, 30, 1); // カーソル位置
  M5.Lcd.setTextColor(WHITE, BLACK); // 文字を白、背景を黒
  // バッテリーの電圧、modeをディスプレイに表示（検証時のみ表示する）
  M5.Lcd.printf("vbat: %4.2fV\r\n", (float)vbat / 100);
  M5.Lcd.printf("mode(A): %d\r\n", btnAStat);
  M5.Lcd.printf("mode(B): %d\r\n", btnBStat);
   
  // Serial output
  /*
  if ( btnBStat == 0 ) {
    Serial.printf("%6.2f,%6.2f,%6.2f\n", gyroX, gyroY, gyroZ);
  } else if ( btnBStat == 1 ) {
    Serial.printf("%5.2f,%5.2f,%5.2f\n", accX, accY, accZ);
  } else if ( btnBStat == 2 ) {
    Serial.printf("%5.2f,%5.2f,%5.2f\n", pitch, roll, yaw);
  } else if ( btnBStat == 3 ) {
    Serial.printf("%5.2f\n", temp);
  } 
  */

  // BLE: Periferal--＞Central on Connect mode
  // notify changed value

   buf[0] = (uint8_t)(aX & 0xff); //5 aXの下位バイト
   buf[1] = (uint8_t)((aX >> 8) & 0xff); //6 aXの上位バイト
   buf[2] = (uint8_t)(aY & 0xff);  //7 aYの下位バイト
   buf[3] = (uint8_t)((aY >> 8) & 0xff); //8 aYの上位バイト
   buf[4] = (uint8_t)(aZ & 0xff); //9 aZの下位バイト
   buf[5] = (uint8_t)((aZ >> 8) & 0xff); //10 aZの上位バイト
   buf[6] = (uint8_t)(vbat & 0xff); //11 電池電圧の下位バイト
   buf[7] = (uint8_t)((vbat >> 8) & 0xff); //12 電池電圧の上位バイト
   buf[8] = (uint8_t)(btnAStat & 0xff); //13 btnAStatの下位バイト
   buf[9] = (uint8_t)((btnAStat >> 8) & 0xff); //14 btnAStatの上位バイト
   buf[10]= (uint8_t)(btnBStat & 0xff); //15 btnAStatの下位バイト
   buf[11]= (uint8_t)((btnBStat >> 8) & 0xff); //16 btnAStatの上位バイト

   //for (int i=0;i<11;i++){
   //  Serial.printf("%d ",buf[i]);
   //}
   //Serial.println(buf[11]);
   //uint16_t vbat2 = (buf[7] << 8) + buf[6];
   //Serial.println(vbat, DEC);
   //Serial.println(vbat2, DEC);
   

   if(deviceConnected) {
        M5.Lcd.setCursor(0, 0, 1); // カーソル位置   
        M5.Lcd.setTextColor(GREEN, BLACK); // 文字を白、背景を黒
        M5.Lcd.printf(">connected   ");
        
        pCharacteristic->setValue(buf,sizeof buf);
        pCharacteristic->notify();
        delay(10); // bluetooth stack will go into congestion, if too many packets are sent, in 6 hours test i was able to go as low as 3ms
   }
   // disconnecting
   if (!deviceConnected && oldDeviceConnected) {
   
        M5.Lcd.setCursor(0, 0, 1); // カーソル位置    
        M5.Lcd.setTextColor(RED, BLACK); // 文字を白、背景を黒
        M5.Lcd.printf("<disconnected");
        
        delay(500); // give the bluetooth stack the chance to get things ready
        pServer->startAdvertising(); // restart advertising
        Serial.println("start advertising");
        oldDeviceConnected = deviceConnected;
   }
   // connecting
   if (deviceConnected && !oldDeviceConnected) {
        // do stuff here on connecting
        oldDeviceConnected = deviceConnected;
   }
  
   delay(10);

}
