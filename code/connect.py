from bluepy import btle
import time
import sys 

# address to coonect 
MAC_ADDRESS="50:02:91:a2:1a:fa"
#UUIDs
SERVICE_UUID           = "00000010-0000-1000-8000-00805F9B34FB"
CHARACTERISTIC_UUID_RX = "00000011-0000-1000-8000-00805F9B34FB"
CHARACTERISTIC_UUID_TX = "00000012-0000-1000-8000-00805F9B34FB"

# 通知をハンドルするデリゲート用クラス
class MyDelegate(btle.DefaultDelegate):
    def __init__(self, params):
        btle.DefaultDelegate.__init__(self)
        # ... initialise here
    def handleNotification(self, cHandle, data):
        print(data)

def connect_with_retry(p: btle.Peripheral, macaddr: str, retry_count=1) -> bool:
    for _ in range(retry_count):
        try:
            p.connect(macaddr)
            print("接続に成功しました。")
            return True
        except btle.BTLEDisconnectError:
            print("タイムアウトしました。接続を再試行します。")
            time.sleep(1)
    else:
        print("規定のリトライ回数を超過しました。接続に失敗しました。")
        return False

# connect
peripheral = btle.Peripheral()

# 通知ハンドル用クラスをセット
peripheral.withDelegate( MyDelegate(params=0) )
if connect_with_retry(peripheral, MAC_ADDRESS , retry_count=5) == False:
    print("接続エラー")

try:
    while True: 
        # ここに、Ctrl-C で止めたい継続処理を書く
        # サービスを取得（サービスは1つだけの想定）
        svc = peripheral.getServiceByUUID( SERVICE_UUID )

        # キャラクタリスティックを取得
        # Periferalから
        for characteristic in peripheral.getCharacteristics(uuid=CHARACTERISTIC_UUID_TX):
            print(f'  UUID：{characteristic.read()}')

        # Periferalへ（encodeが必要)
        for characteristic in peripheral.getCharacteristics(uuid=CHARACTERISTIC_UUID_RX):
            characteristic.write('test'.encode('utf-8')) 

        '''
        for service in peripheral.getServices():
            print(f'UUID：{service.uuid}')
            for characteristic in service.getCharacteristics():
                print(f'  UUID：{characteristic.uuid}')
                print(f'    ハンドル：{characteristic.getHandle()}')
                print(f'    プロパティ：{characteristic.propertiesToString()}')
        '''
except KeyboardInterrupt:
    # Ctrl-C で抜ける処理を書く
    print("")
    peripheral.disconnect()
    print("periferal disconnected...")
    sys.exit(0)

